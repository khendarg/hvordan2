#!/usr/bin/env python

import argparse
import libquod
import numpy as np
from Bio import SeqIO

def preview(*arrs):
	import matplotlib.pyplot as plt
	for arr in arrs:
		plt.plot(arr)
	plt.show()
def impreview(*arrs, zoom=1, vmin=None, vmax=None):
	import matplotlib.pyplot as plt
	fig, ax = plt.subplots()
	for arr in arrs:
		fig.colorbar(ax.imshow(arr, extent=(0, arr.shape[1] * zoom, arr.shape[0] * zoom, 0), cmap="coolwarm", vmin=vmin, vmax=vmax))
	plt.show()
def fancypreview(arr1, arr2, correl, zoom, vmin=None, vmax=None):
	import matplotlib.pyplot as plt
	fig = plt.figure()
	ax1 = fig.add_subplot(223)
	ax2 = fig.add_subplot(222)
	axc = fig.add_subplot(223)
	for arr in arrs:
		fig.colorbar(ax.imshow(arr, extent=(0, arr.shape[1] * zoom, arr.shape[0] * zoom, 0), cmap="coolwarm", vmin=vmin, vmax=vmax))
	plt.show()
	

def boxcorrel(arr1, arr2, window):
	xcmatrix = np.zeros((len(arr1)//window, len(arr2)//window))
	for x1 in np.arange(0, len(arr1), window):
		r = x1 // window
		for x2 in np.arange(0, len(arr2), window):
			c = x2 // window
			if len(arr1[x1:x1+window]) != len(arr2[x2:x2+window]): continue
			xcmatrix[r,c] = np.correlate(arr1[x1:x1 + window], arr2[x2:x2 + window])
	return xcmatrix

def main(args):
	sequences1 = list(SeqIO.parse(args.query, "fasta"))
	sequences2 = list(SeqIO.parse(args.subject, "fasta"))

	quodtype = libquod.entities.Parser.parse_mode(args.mode)

	quodkwargs = {}
	quodkwargs["kernel"] = args.quodkernel
	quodkwargs["window"] = args.quodwindow

	ents1 = {}
	for record1 in sequences1:
		ents1[record1.id] = quodtype.compute(record1, **quodkwargs)
	ents2 = {}
	for record2 in sequences2:
		ents2[record2.id] = quodtype.compute(record2, **quodkwargs)

	#preview(*[ents1[name1].Y for name1 in ents1])

	for entname1 in ents1:
		ent1 = ents1[entname1]
		for entname2 in ents2:
			ent2 = ents2[entname2]

			xcmatrix = boxcorrel(ent1.Y, ent2.Y, args.window)

			impreview(xcmatrix, zoom=args.window, vmin=-2*args.window, vmax=2*args.window)
					
			#print(ent1.Y.shape, ent2.Y.shape)
			#correl = np.correlate(ent1.Y, ent2.Y)
			#print(correl.shape)
			#preview(ent1.Y, ent2.Y, correl)
			#preview(correl)
			#print(correl)
			

if __name__ == "__main__":
	parser = argparse.ArgumentParser()

	parser.add_argument("-g", type=float, default=0., help="Gap opening penalty for hydrophilic segments")
	parser.add_argument("-e", type=float, default=0., help="Gap extension penalty for hydrophilic segments")
	parser.add_argument("-G", type=float, default=0., help="Gap opening penalty for hydrophobic segments")
	parser.add_argument("-E", type=float, default=0., help="Gap extension penalty for hydrophobic segments")

	parser.add_argument("--mode", default="hydro", help="Which mode to use (default: hydro)")

	parser.add_argument("--quodkernel", default="hann", help="Which kernel to use (default: hann)")
	parser.add_argument("--quodwindow", type=int, default=19, help="Window width for average hydropathy/entropy calculation (default: 19)")

	parser.add_argument("--window", type=int, default=15, help="Window width for cross-correlation matrix")

	parser.add_argument("query")
	parser.add_argument("subject")

	args = parser.parse_args()

	main(args)
