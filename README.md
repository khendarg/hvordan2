# hvordan2

Saier Lab tools for plotting sequence-related data

## hvordan
HTML Visualization Of Randomized Domain Alignment Networks

This script generates HTML reports of individual Protocol2 results.

## Summary
This script generates HTML reports with hydropathy plots and representations of TCDB BLAST hits (mostly replicating the [TCDB BLAST tool](http://www.tcdb.org/progs/blast.php)) for _Protocol2_ results. 
This can be done in bulk on entire sets of _Protocol2_ results, on specific genes, on specific pairs of genes, or on specific ranges of GSAT Z-scores.

## Dependencies
The following programs need to be available in your path for this program to run properly:

1. **_blast+ 2.11.0+_**  
Older versions of blast may require minor adaptations. 
Visit the
 [download site](https://blast.ncbi.nlm.nih.gov/Blast.cgi?PAGE_TYPE=BlastDocs&DOC_TYPE=Download). 

2. **_TCDB protein database_**  
Given that blastdbcmd runs locally, the TCDB database must be available locally through the environment variable _$BLASTDB_. 
If possible, use [extractFamily.pl](https://github.com/SaierLaboratory/TCDBtools/blob/master/scripts/extractFamily.pl) to download the TCDB BLAST database:
```bash
extractFamily.pl -i all -f blast
```
Otherwise, you can manually download [all TCDB sequences](http://www.tcdb.org/public/tcdb) from the TCDB and run ```makeblastdb -in tcdb.fasta -out tcdb -dbtype prot```, but this script was not tested with such manually downloaded BLAST databases. 

3. **_Python 3.7+_**  
Visit the [official website](https://www.python.org/). 
This program was not tested with older versions of Python 3.

4. **_Matplotlib 3.1.1-3.5.2+_**  
Visit the [official website](https://matplotlib.org/).
While not required for hvordan.py itself, Matplotlib is required for the graph plotting modules, and automating graph generation is the entire point of the script.

5. **_Biopython 1.75-1.79+_**
Visit the [official website](http://biopython.org/).
For now, index-finding for the ABCD plots will rely on pairwise2 on account of indices not being available on report.tbl.

## quod
Quantitative Visualization of On-membrane Domains

Makes average hydropathy graphs from sequences and sequence-containing files

## Summary
This script generates average hydropathy plots with arbitrary resolution in a variety of commonly used formats. 
This can be done on multiple sequences.
This tool mostly replicates [WHAT](http://biotools.tcdb.org/barwhat2.html).

## Dependencies
The following programs need to be available in your path for this program to run properly:

1. **_Python 3.7+_**  
Visit the [official website](https://www.python.org/). 
This program was not tested with older versions of Python 3.

2. **_Matplotlib 3.1.1-3.5.2+_**  
Visit the [official website](https://matplotlib.org/).

3. **_Biopython 1.75-1.79+_**
Visit the [official website](https://biopython.org/).

## Command line options
The following options are available. 
You can also run the script without arguments (or with -h or --help) to display the options:

### Positional arguments:
`[INFILE1 [INFILE2 [INFILE3 ...[INFILEN]]]]` Sequence file(s) to read in. Defaults to stdin. If QUOD doesn't seem to be doing anything and exits on `^D`, it may be missing an infile or an optional argument may have eaten the infile. Adding the `asis:` prefix to positional arguments (e.g. `asis:ACDEFGHIKLMNPQRSTVWY`) instructs QUOD to treat the rest of the argument as a raw sequence (viz. `ACDEFGHIKLMNPQRSTVWY`).

### Figure arguments:
`-l TITLE` Give the figure a title

`--xlabel XLABEL --ylabel YLABEL` Set axis labels

`--axis-font` Set axis label size (pt)

`--tick-font` Set tick label size (pt)

`--height` Set plot height, most likely in inches. Default: 5.5

`--width` Set plot width, most likely in inches. Default: dynamic

### Meta arguments:
`-s` Interpret all positional arguments as raw sequences. As this argument may be deprecated in the future, using the asis: prefix on positional arguments is recommended.

`-v` Verbose output, unhide all warnings.

`--multi MODE` How to handle multiple sequences. Accepts `stack` (default), which prints plot elements over each other, and `frag`, which aligns fragments onto full sequences and accepts sequences in fragment1, full1, fragment2, full2... fragment3, full3... order.

### Calculation arguments:
`--angle ANGLE` Angle for amphipathicity calculations. Defaults to 100 degrees.

`--kernel KERNEL` Smoothing kernel(s) to use. Implemented kernels are `flat` (default), `gauss`, `hann`, `triangular`, `quadratic`, and `quartic`. Separate kernels to be used on the same axes with "+", e.g. `--kernel flat+hann`.

`--mode MODE` What kinds of plots to produce from `hydro` (default), `compositional`, `conformational`, `solventaccess`, `psipred`, and `hmmtopemission`. Accepts multiple arguments. If plotting things on the same axes, use "+" to separate plot modes.

`--window WINDOW` Window widths for smoothing kernels. Odd numbers are recommended. Default values are 19 for hydrophobicity/amphipathicity/hmmtopemission, 40 for compositional/conformational, and 1 for psipred.

### TMS arguments:
`--add-tms start-end[:color]` Space-separated list of dash-delimited TMSs to add with optional per-TMS coloring. Matplotlib-recognized color names are accepted, as are hex triplets/quadruplets in RGB[A] order.

`--load-tms (+seqid) filename` Load TMSs from disk for a specific seqid.

`--no-tms (+seqid)` Clear TMSs for specified sequences and skip running HMMTOP for them.

### Region/domain arguments:
`--add-region start-end[:color][:"label"]` Draw regions, typically used for marking Pfam domains.

`--region-font FONT` Font size for region markers in points (default: 6)

`--mark (+id):(resn1,resn2,resn3(:color))` Add circular markers on specified curves for comma-separated regular expressions or simple residues.

### Miscellaneous plot feature arguments:
`-b X1 X2 X3` Draw vertical bars at these positions.

`-w start-end[:y[:+-[:text]]]` Surround intervals in vertical bars with wedges pointing in. 

`-W x[:y[:+-[:scale]]]` Manually draw single bars with wedges pointing right for positive scale values and left for negative scale values.

`--grid` Enable the major grid.

`--legend` Enable legend.

`--xlim LEFT RIGHT` Set x limits.

`--ylim BOTTOM TOP` Set y limits.

`--xticks SPACING` Set x tick spacing.

`--yticks SPACING` Set x tick spacing.
