import os
import sys
import numpy as np
import matplotlib.pyplot as plt

class HvordanZSelector(object):
	p2dir = None
	mode = 'normal'
	zmin = None
	zmax = None
	zlist = None

	lastmin = None
	lastmax = None
	minents = None
	maxents = None

	fig = None
	ax = None

	binsize = 1.0
	def __init__(self, p2dir, binsize=1.0):
		self.p2dir = p2dir
		self.binsize = binsize

		self.minents = []
		self.maxents = []

	def get_freqs(self):
		freqs = {}
		zlist = []
		def _bin_results(fh, resultsdict, column=3):
			for l in fh:
				if l.startswith('#'): continue
				elif l.startswith('Subject'): continue
				else:
					sl = l.split('\t')
					if float(sl[3]) in resultsdict: resultsdict[float(sl[3])] += 1
					else: resultsdict[float(sl[3])] = 1
			return resultsdict
		def _bin_results(fh, zlist, column=3):
			for l in fh:
				if l.startswith('#'): continue
				elif l.startswith('Subject'): continue
				else:
					sl = l.split('\t')
					zlist.append(float(sl[3]))
			return zlist

		for path in self.p2dir:
			if path.endswith('report.tbl'):
				with open(path) as fh: additions = _bin_results(fh, zlist)
			elif os.path.isdir(path):
				for subpath in os.listdir(path): 
					if subpath.endswith('report.tbl'):
						with open('{}/{}'.format(path, subpath)) as fh: additions = _bin_results(fh, zlist)
					elif os.path.isdir('{}/{}'.format(path, subpath)):
						for subsubpath in os.listdir('{}/{}'.format(path, subpath)): 
							if subsubpath.endswith('report.tbl'):
								with open('{}/{}/{}'.format(path, subpath, subsubpath)) as fh: additions = _bin_results(fh, zlist)

		self.zlist = np.array(zlist)
		
		return zlist
		z = []
		counts = []
		[(z.append(k), counts.append(freqs[k])) for k in sorted(freqs)]
		return z, counts

	def count_valid(self, zlim=None):
		if zlim is None: zlim = (self.zmin, self.zmax)

		if zlim[0] is None: valid = self.zlist[:]
		else: valid = self.zlist[self.zlist >= zlim[0]]

		if zlim[1]  is None: valid = valid[:]
		else: valid = valid[valid < zlim[1]]

		return len(valid)

	def print_help(self):
		print('''
CONTROLS
========

d - Clear current boundary (Use with SELECT ZMIN/ZMAX mode)
k - Toggle log/linear x-axis
l - Toggle log/linear y-axis
s - Export plot to (static) image
x - Enter SELECT ZMIN mode
c - Count Protocol2 hits satisfying current bounds
z - Enter SELECT ZMAX mode
? - Pull up this help page
ESC - Enter normal mode, which does absolutely nothing
''', file=sys.stderr)


	def onkeydown(self, event):
		if 'drag' in self.mode: return

		if event.key == '?': self.print_help()

		elif event.key == 'z': 
			self.mode = 'zmin'
			self.update_title()
		elif event.key == 'x': 
			self.mode = 'zmax'
			self.update_title()
		elif event.key == 'c': 
			print('{} alignments satisfy {} <= Z < {} ({} above, {} under)'.format(
				self.count_valid(),
				self.zmin, self.zmax,
				self.count_valid((self.zmax if self.zmax is not None else self.zmin, None)),
				self.count_valid((None, self.zmin if self.zmin is not None else self.zmax)),
			))

		elif event.key == 'd':
			if self.mode == 'zmin': 
				self.zmin = None
				for ent in self.minents: ent.remove()
				self.minents = []
			elif self.mode == 'zmax': 
				self.zmax = None
				for ent in self.maxents: ent.remove()
				self.maxents = []
			self.fig.canvas.draw()
			self.update_title()

		elif event.key == 'escape':
			self.mode = 'normal'
			self.update_title()

	def onmousedown(self, event): 
		if not event.inaxes: return

		if self.mode in ('zmin', 'zmax'): 
			if self.mode == 'zmin': self.update_markers(event.xdata)
			elif self.mode == 'zmax': self.update_markers(event.xdata)
			self.mode += 'drag'

		else: return
			
	def onmouseup(self, event): 
		if not event.inaxes: return

		if self.mode.startswith('z'):
			self.mode = self.mode[:-4]
	def onmousemove(self, event): 
		if not event.inaxes: return

		if not self.mode.endswith('drag'): return

		self.update_markers(event.xdata)

	def update_markers(self, newx):

		roundx = np.round(newx / self.binsize) * self.binsize

		if self.mode.startswith('zmin'):
			if self.lastmin is not None:
				if roundx == self.lastmin: return
				else:
					for ent in self.minents: ent.remove()
					self.minents = []
				
			self.zmin = roundx
			self.lastmin = roundx
			self.minents.append(self.ax.axvline(roundx, color='k'))
			self.minents.append(self.ax.annotate('', 
				(roundx, np.mean(self.ax.get_ylim())),
				(roundx + self.binsize/2, np.mean(self.ax.get_ylim())),
				arrowprops={'arrowstyle':'->', 'color':'k'}))
			self.fig.canvas.draw()

		elif self.mode.startswith('zmax'):
			if self.lastmax is not None:
				if roundx == self.lastmax: return
				else:
					for ent in self.maxents: ent.remove()
					self.maxents = []

			self.zmax = roundx
			self.lastmax = roundx
			self.maxents.append(self.ax.axvline(roundx, color='k'))
			self.maxents.append(self.ax.annotate('', 
				(roundx, np.mean(self.ax.get_ylim())),
				(roundx - self.binsize/2, np.mean(self.ax.get_ylim())),
				arrowprops={'arrowstyle':'->', 'color':'k'}))
			self.fig.canvas.draw()

		

	def update_title(self):
		if self.mode == 'zmin': title = 'SELECT ZMIN mode'
		elif self.mode == 'zmax': title = 'SELECT ZMAX mode'
		else: title = 'NORMAL mode'
		self.fig.canvas.set_window_title(title)
					
	def run(self):
		zlist = self.get_freqs()
		self.fig = plt.figure()
		self.ax = self.fig.gca()
		self.ax.figure.canvas.mpl_connect('key_press_event', self.onkeydown)
		self.ax.figure.canvas.mpl_connect('button_press_event', self.onmousedown)
		self.ax.figure.canvas.mpl_connect('button_release_event', self.onmouseup)
		self.ax.figure.canvas.mpl_connect('motion_notify_event', self.onmousemove)

		self.ax.hist(zlist, np.arange(min(zlist), max(zlist)+self.binsize, self.binsize))
		#self.ax.semilogy(True)
		self.ax.set_xlim(min(zlist), max(zlist) + self.binsize)
		self.ax.set_xlabel('Score')
		self.ax.set_ylabel('Count')
		self.ax.set_title('Total protocol2 results (n={})'.format(len(self.zlist)))
		self.update_title()
		print('Press [?] to list keyboard shortcuts/hotkeys. Press `q\' when the number of alignments is reasonable.', file=sys.stderr)

		plt.show()

	def get_zlim(self): return self.zmin, self.zmax

