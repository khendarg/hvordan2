Documentation for application: _multiquod.py_

<!-- PROGRAM STUFF -->
## Summary

This application is intended to provide a scalable interface to libquod for generating publication-quality figures.
It currently supports hydropathy, entropy, and TMS prediction plotting in addition to allowing a number of commonly used user-defined plot objects.
The config file syntax has been drastically simplified from the previous version of multiquod, and the minimal config file is now two lines rather than five or so.

## Dependencies

***Python 3.6.1+***

***Matplotlib 3.1.1+***

***Numpy 1.18.1+***

***Astropy 3.2.2+***

HMMTOP

## Sections

`[figure]` and `[subplots.NAME]` are special section names used for figure-wide options and subplot-specific options respectively. 
The `[figure]` section is optional, and the default parameters have been optimized for a typical QUOD plot.
The `[subplots.NAME]` sections are mandatory and define what each subplot contains.

<!-- FIGURE STUFF -->
## [figure]

### **dpi** (Integer)

DPI of figure.
Only applied to figures saved using the `save` configuration option.

### **equal** (Boolean)

Distribute column width equally for multicolumn rows.

### **height** (Float)

Figure height.

### **rows** (String)

Use to define the subplot layout of a figure with a multiline whitespace-separated specification like so:

```
rows = a b
       c d
        e
```

### **save** (String)

Save the rendered figure to disk according to the value of `save`

### **width** (Float)

Figure width.

<!-- SUBPLOT STUFF -->
## [subplots.NAME]

### **addregion** (String)

Draw a colored rectangle and some text. Accepts the following keyword arguments:

"Required" arguments:
 - label (String): Text to place on rectangle
 - text (String): Text to place on rectangle
 - x (String): Comma-separated colon-delimited ranges. (1-40 80-90 -> 1:40,80:90)
 - y (String): Colon-delimited y range (2-2.25 -> 2:2.25)

"Optional" arguments:
 - alpha (Float): Opacity of rectangle. Unset by default but works out to 1.0.
 - color or facecolor or fc (String): Color of rectangle. Recognized formats include color names (e.g. 'r', 'red', 'g', 'limegreen') and three or four-byte hexcodes ('#00ff00', '#00ff0040'). Defaults to red.
 - fontsize (Float): Font size for text. Unset by default and falls back to Matplotlib defaults.
 - halign (String): Horizontal alignment of text relative to the colored rectangle. Accepts "left", "center", and "right". Defaults to "center".
 - valign (String): Vertical alignment of text relative to the colored rectangle. Accepts "top", "center", and "bottom". Defaults to "top".
 - textcolor (String): Color of text. Defaults to white inside sufficiently dark rectangles and black elsewhere.

The current syntax is incredibly tedious and may soon be deprecated in favor of a more shell-like syntax along the lines of `1-40x2-3.5:'Some domain' --color red --alpha 1.0 --valign c --halign c`

Each entity should be defined on a separate line like so:

```
addregion: label:"Region 1" x:40:80 y:1:1.5
  x:60:70,110:150 y:2:2.5 label:"Discontiguous domain"
```

### **addtms** (String)

Shade areas defined as intervals along the x axis. 
As with `addregion`, `addtms` accepts one complete definition per line for cases in which multiple colors of horizontal bars are needed. 
The current syntax is incredibly tedious and may soon be deprecated in favor of a more shell-like syntax along the lines of `1-40 80-90 --color red --alpha 0.25`

"Required" arguments:
 - label (String): Text to place on TMS bar (if any)
 - text (String): Text to place on TMS bar (if any)
 - x (String): Comma-separated colon-delimited ranges. (1-40 80-90 -> 1:40,80:90)
 - y (Float): Y-coordinate at which to place label (if any). Defaults to 2.0.

"Optional" arguments:
 - alpha (Float): Opacity of rectangle. Defaults to 0.25.
 - color or facecolor or fc (String): Color of rectangle. Recognized formats include color names (e.g. 'r', 'red', 'g', 'limegreen') and three or four-byte hexcodes ('#00ff00', '#00ff0040'). Defaults to orange.
 - fontsize (Float): Font size for text (if any). The Matplotlib default is 10.0.
 - halign (String): Horizontal alignment of text relative to the colored rectangle. Accepts "left", "center", and "right".
 - textcolor (String): Color of text. Defaults to white inside sufficiently dark and opaque rectangles and black elsewhere.

Each entity should be defined on a separate line like so:

```
addtms: 20:40,80:100 fc:orange
  22:41,79:101 fc:cyan
```

### **addwall** (String)

Draw vertical bars bounding a region with optional wedges or span arrows. Accepts the following keyword arguments:

"Required" arguments:
 - x (String): Comma-separated colon-delimited ranges or coordinates for bars. (1-40 80-90 3 -> 1:40,80:90,3)
 - y (String): Y coordinate of wedges or span arrows.

"Optional" arguments:
 - alpha (Float): Opacity of plot elements. Unset by default but works out to 1.0.
 - fontsize (Float): Font size for text. Unset by default and falls back to Matplotlib defaults.
 - halign (String): Horizontal alignment of text relative to the span arrows. Accepts "left", "center", and "right". Defaults to "center".
 - valign (String): Vertical alignment of text relative to the colored rectangle. Accepts "top", "center", and "bottom". Defaults to "top".
 - textcolor (String): Color of text. Defaults to white inside sufficiently dark rectangles and black elsewhere.
 - ypos (String): +, -, or +-. Sets which quadrants get bars relative to the x axis.

Each entity should be defined on a separate line like so:

```
#new version with span arrows and optional text:
addwalls: x:40:80 y:1.732 ypos:+- label:"Something interesting"
#old version with inward-facing wedges
  x:100 y:1.732 ypos:+ scale:1
  x:120 y:1.732 ypos:+ scale:-1

  x:110 y:-1.732 ypos:- scale:1
  x:125 y:-1.732 ypos:- scale:-1
```


### **ctitle** (String)

Centered subplot title. Effectively equivalent to setting **title**.

### **kernel** (String)

Whitespace-separated list of kernels to use. "-" and "\_" are recognized as aliases for "flat", the moving average kernel.
This allows definitions like the following for a plot in which the first curve uses the default moving average, the second uses a Hann cosine kernel, the third uses the default moving average kernel, and the last uses a triangular kernel:

```
kernels: - hann - triang
```

### **legend** (Boolean)

Display a legend for the curves on this subplot

### **length** (String)

Explicitly set the effective length of the subplot for dynamic width calculation.

### **load** (String)

Whitespace-separated files to read in.
The special tokens `--msa` (NYI) and `--frag` instruct Multiquod to treat the following sequences as multiple sequence alignments or pairwise alignment fragments respectively.

```
load: --frag alignedfragment1.faa fullsequence1.faa --frag alignedfragment2.faa fullsequence2.faa othersequence.faa
```

### **ltitle** (String)

Left-aligned subplot title.

### **mode** (String)

Whitespace-separated list of modes to render.

### **notms** (Integers)

Have multiquod skip automatic TMS predictions for a whitespace-separated list of sequence indices or the `all` keyword.

### **rtitle** (String)

### **title** (String)

Subplot title in the usual spot (centered).

### **window** (Integer)

### **xlabel** (String)

X-axis label

### **xlim** (Integers)

Manually set x limits for this subplot.

### **xscale** (Float)

Set the distance between x ticks for this subplot.

### **ylabel** (String)

Y-axis label

### **ylim** (Integers)

Manually set y limits for selected subplot.

### **yscale** (Float)

Set the distance between x ticks for this subplot.

<!-- ENTITY STUFF -->
## ENTITYNAME.PROPNAME (varies)

Set an entity-specific (e.g. hydropathy0, hydropathy1, hmmtop1) property like linewidth, edgecolor, facecolor, or fontsize.
Global properties may be set in [figure] with the same syntax, but subplot-specific property-setting directives will take precedence over globally set property-setting directives.

## Index of known entity names

### Explicit entities

Explicit entities exist as libquod/multiquod-level abstractions.
Most data and plottables are explicit entities.

 - `amphi`: Colored curve plotting amphipathicity, i.e. the first moment of the hydropathy
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Amphipathicity".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `charge`: Colored curve plotting mean local charge at pH 7. Other pH values are not currently implemented
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Charge".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `composition`: Colored curve plotting local compositional entropy, i.e. the Shannon entropy of the surrounding residues
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Compositional entropy".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `conform`: Colored curve plotting local torsional entropy, i.e. the mean degrees of torsional freedom available to side chains of neighboring residues
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Conformational entropy".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `entropy`: Colored curve plotting local compositional entropy, i.e. the Shannon entropy of the surrounding residues
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Compositional entropy".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `hmmtop`: Translucent, colored bars with no text typically used to represent transmembrane segments or large intervals of interest
      - alpha (Float): Opacity of spans. Defaults to 0.25.
      - centers (Boolean): Plot TMS centers instead of TMS spans
      - edgecolor (String): Color of border.
      - facecolor (String): Color of spans. Defaults to orange.
      - label (String): Unset by default and will not appear in legends.

 - `hydropathy`: Colored curve typically plotting mean local hydropathy
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Hydropathy".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `identity`: Colored curve plotting mean identity given a pair of sequences
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Identity".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

 - `region`: Opaque, colored rectangles with text annotations typically used to represent domains
      - facecolor (String): Color of region marker. Defaults to "red".
      - fontsize (Float): Size of region label in points.
      - halign ("left", "center", or "right"): Horizontal alignment of region label. Defaults to "center".
      - label (String): Unset by default.
      - textcolor (String): Unset by default but picks either black or white depending on .facecolor.
      - valign ("top", "center", or "bottom"): Vertical alignment of region label. Defaults to "top".

 - `torsion`: Colored curve plotting local torsional entropy, i.e. the mean degrees of torsional freedom available to side chains of neighboring residues
      - edgecolor (String): Color of curve
      - label (String): Label for curve. Used if rendering figure legends. Defaults to "Torsion".
      - linewidth (Float): Width of curve in points. Defaults to 1.5 on standard Matplotlib installations.

#### A note on regular expressions

Patterns formatted as `/PATTERN/.PROPNAME` (or the shortcut `^PATTERN.PROPNAME` for prefixes) will be processed as regular expressions.
This allows the use of directives like `^hydro.edgecolor` to set the edge color of all hydropathy objects or `/.*2$/.edgecolor` to set the edgecolor of all objects with identifiers ending in `2`.

### Implicit entities

Most annotations, titles, and axis properties are implicit entities, i.e. there does not exist an explicit libquod/multiquod-level class representing them.
This is a list of entities for which attributes are exposed:

 - `ctitle`: Center subplot title. Commonly set attributes include `ctitle.fontsize`. Multiquod simplifies a number of attributes to simplify multi-title subplots, and most keyword arguments for [matplotlib.axes.Axes.set_title](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_title.html) are ignored.
      - fontsize: Title text size

 - `label`: X-axis and Y-axis labels. Commonly set attributes include `label.fontsize` and `label.loc`. See 
[matplotlib.axes.Axes.set_xlabel](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_xlabel.html)
and
[matplotlib.axes.Axes.set_ylabel](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_ylabel.html).
      - color (String): Color for axis label
      - fontsize (Float): Size of axis label

 - `ltitle`: Left subplot title. Commonly set attributes include `ltitle.fontsize`. Multiquod simplifies a number of attributes to simplify multi-title subplots, and most keyword arguments for [matplotlib.axes.Axes.set_title](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_title.html) are ignored.
      - fontsize: Title text size

 - `rtitle`: Left subplot title. Commonly set attributes include `rtitle.fontsize`. Multiquod simplifies a number of attributes to simplify multi-title subplots, and most keyword arguments for [matplotlib.axes.Axes.set_title](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_title.html) are ignored.
      - fontsize: Title text size

 - `ticks`: X and Y ticks. Commonly set attributes include `ticks.labelsize` (not .fontsize) and `ticks.labelrotation`. See [matplotlib.axes.Axes.tick_params](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.tick_params.html).
      - color (String): Color for everything
      - labelsize (Float): Font size for tick labels

 - `title`: Subplot title. Commonly set attributes include `title.fontsize`. Multiquod simplifies a number of attributes to simplify multi-title subplots, and most keyword arguments for [matplotlib.axes.Axes.set_title](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_title.html) are ignored.
      - fontsize: Title text size

 - `xlabel`: X-axis label. Commonly set attributes include `xlabel.fontsize` and `xlabel.loc`. See 
[matplotlib.axes.Axes.set_xlabel](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_xlabel.html).
      - color (String): Color for axis label
      - fontsize (Float): Size of axis label

 - `ylabel`: Y-axis label. Commonly set attributes include `ylabel.fontsize` and `ylabel.loc`. See 
[matplotlib.axes.Axes.set_ylabel](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.set_ylabel.html).
      - color (String): Color for axis label
      - fontsize (Float): Size of axis label

 - `xticks`: X ticks. Commonly set attributes include `xticks.labelsize` (not .fontsize) and `xticks.labelrotation`. See [matplotlib.axes.Axes.tick_params](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.tick_params.html).
      - color (String): Color for everything
      - labelsize (Float): Font size for tick labels

 - `yticks`: X ticks. Commonly set attributes include `yticks.labelsize` (not .fontsize) and `yticks.labelrotation`. See [matplotlib.axes.Axes.tick_params](https://matplotlib.org/3.3.2/api/_as_gen/matplotlib.axes.Axes.tick_params.html).
      - color (String): Color for everything
      - labelsize (Float): Font size for tick labels
