import io

import Bio.Blast.NCBIXML

def autoparse(s):
    try: return int(s)
    except ValueError:
        try: return float(s)
        except ValueError:
            return s

def dump_fasta_m10(text):
    """

    #FIXME: find a good notation for this for turn into an object
    Returns a list of dictionaries in the following format:

    {
        "runtime": %runtimevalues,
        "alignments": [
            {
                "sequences":[]
            }
    }

    """
    out = []
    mode = "preamble"
    for l in text.split("\n"):
        if mode == "preamble":
            if l.startswith(">>>"):
                mode = "alignment_runtime"
                out.append({"runtime":{}, "alignments":[]})
        elif mode.startswith("alignment"):
            if l.startswith(";"):
                key = l[2:l.find(":")]
                value = autoparse(l[l.find(":")+1:].strip())

                if mode == "alignment_runtime":
                    out[-1]["runtime"][key] = value
                elif mode == "alignment_pair":
                    out[-1]["alignments"][-1][key] = value
                elif mode == "alignment_sequence":
                    if key == "al_cons":
                        mode = "alignment_conservation"
                        out[-1]["alignments"][-1]["conservation"] = ""

                    else: 
                        out[-1]["alignments"][-1]["sequences"][-1][key] = value

            elif l.startswith(">>><<<"):
                #FIXME: check if this just ends the alignment later
                #NOTE: to avoid rule-order bugs, maintain this order of conditions
                break

            elif l.startswith(">>"):
                mode = "alignment_pair"
                out[-1]["alignments"].append({"id":l[2:], "sequences":[]})

            elif l.startswith(">"):
                mode = "alignment_sequence"
                out[-1]["alignments"][-1]["sequences"].append({"id":l[1:-3], "seq":""})

            elif mode == "alignment_sequence":
                out[-1]["alignments"][-1]["sequences"][-1]["seq"] += l

            elif mode == "alignment_conservation":
                out[-1]["alignments"][-1]["conservation"] += l

    return out

def _parse_fasta_m10(text):
    obj = dump_fasta_m10(text)
    out = []
    for run in obj: 
        for alignment in run["alignments"]:
            seq1 = alignment["sequences"][0]["seq"]
            start1 = alignment["sequences"][0]["al_start"]
            seq2 = alignment["sequences"][1]["seq"]
            start2 = alignment["sequences"][1]["al_start"]
            cons = alignment["conservation"]
            
            index1 = start1 - 1
            index2 = start2 - 1
            index1 = 0
            index2 = 0


            resi1 = []
            resi2 = []
            for r1, r2, c in zip(seq1, seq2, cons):
                #if c == " ": continue

                if c == ":":
                    resi1.append(index1)
                    resi2.append(index2)

                if r1 != "-": index1 += 1
                if r2 != "-": index2 += 1

            out.append((resi1, resi2))

    return out

def _parse_blast_m5(m5):
    f = io.StringIO(m5)
    out = []
    for record in Bio.Blast.NCBIXML.parse(f):

        for alignment in record.alignments:
            for hsp in alignment.hsps: 

                seq1 = hsp.query
                seq2 = hsp.sbjct
                cons = hsp.match

                index1 = hsp.query_start - 1
                index2 = hsp.sbjct_start - 1

                resi1 = []
                resi2 = []

                for r1, r2, c in zip(seq1, seq2, cons):
                    #if c == " ": continue

                    if c != " ":
                        resi1.append(index1)
                        resi2.append(index2)

                    if r1 != "-": index1 += 1
                    if r2 != "-": index2 += 1
                out.append((resi1, resi2))

    return out
