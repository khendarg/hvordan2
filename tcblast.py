#!/usr/bin/env python
import io
import re
import shlex
import argparse
import tempfile
import subprocess

import numpy as np

from Bio import SeqIO
from Bio.Blast import NCBIXML
import matplotlib.pyplot as plt
import quod
import fasta_grepper

class Tcblast(object):
	query = None
	subject = None
	db = None
	blastargs = None
	def __init__(self, query, db=None, subject=None, outfmt='html', evalue=0.001, blastargs=None):
		self.query = query

		if db is None and subject is None: raise TypeError('At least one of db and subject is required')
		elif db is not None and subject is not None: raise TypeError('No more than one of db and subject is allowed')

		self.db = db
		self.subject = subject

		self.blastargs = blastargs

		self.results = None

		self.outfmt = 'html' #also XML

		self.hmmtop = True

		self.topo = None

		self.evalue = evalue

		self.sequences = {}

	def run(self):
		cmd = ['blastp', '-query', self.query]
		if self.db: cmd.extend(['-db', self.db])
		elif self.subject: cmd.extend(['-subject', self.subject])
		else: raise ValueError('Somehow, your Tcblast object lost both its subject and its db before BLASTing could take place:(')

		cmd.extend(['-outfmt', '5'])

		if self.blastargs: cmd.extend(self.blastargs)
		print(cmd)
		out = subprocess.check_output(cmd)
		blastout = io.BytesIO(out)
		
		self.results = list(NCBIXML.parse(blastout))

		if self.hmmtop: 
			self.fetch_sequences()
			self.compute_hmmtop()

		if self.outfmt in ('text', 'txt', 'plaintext'):
			return self.get_plaintext_report()
		elif self.outfmt in ('html',):
			return self.get_html_report()

	def fetch_sequences(self): 
		sacclist = set()
		for queryrecord in self.results:
			for subjectrecord in queryrecord.alignments:
				sacclist.add(subjectrecord.hit_id)
		
		if self.subject:
			pattern = '|'.join(['^' + sacc + '$' for sacc in sacclist])
			with open(self.subject) as fh:
				[self.sequences.update({record.id:record}) for record in fasta_grepper.main(pattern, fh)]
		elif self.db:
			intf = tempfile.NamedTemporaryFile('w+')
			[intf.write(sacc + '\n') for sacc in sacclist]
			cmd = ['blastdbcmd', '-target_only', '-entry_batch', intf.name]
			out = subprocess.check_output(cmd)
			outf = io.BytesIO(out)

			sequences = SeqIO.parse(outf, 'fasta')

			[self.sequences.update({record.id:record}) for record in sequences]
		else: raise Exception('Somehow, your Tcblast object lost its subject and its db before FASTA retrieval could take place :(')

		for record in SeqIO.parse(self.query, 'fasta'):
			record.id = 'query-{}'.format(record.id)
			self.sequences.update({record.id:record})

		return self.sequences
			
	def compute_hmmtop(self):
		tf = tempfile.NamedTemporaryFile('w+')

		SeqIO.write([self.sequences[acc] for acc in self.sequences], tf, 'fasta')
		tf.flush()

		cmd = ['hmmtop', '-if={}'.format(tf.name), '-pi=spred', '-is=pseudo', '-sf=FAS']
		out = subprocess.check_output(cmd).decode('utf-8')

		self.topo = {}
		for sacc, line in zip(self.sequences, out.split('\n')):
			if not line.strip(): continue
			indices = re.findall('(?:(?:[0-9]+\s*)+)$', line)[0]
			indices = [int(x) for x in indices.strip().split()]
			spans = [indices[i:i+2] for i in range(1, len(indices), 2)]
			self.topo[sacc] = spans
		return self.topo

	def get_mismatches(self, query, midline, offset=0):
		resi = offset
		spans = []
		for i, (q, m) in enumerate(zip(query, midline)):
			if q != '-': 
				resi += 1

				if m == ' ':
					if not spans: spans.append([resi, resi])
					elif resi == spans[-1][-1] + 1: spans[-1][-1] = resi 
					else: spans.append([resi, resi])
		return spans

	def generate_overview_plot(self, qacc=None):
		fig = plt.figure()
		fig.set_tight_layout(True)
		ax = fig.gca()
		if self.hmmtop:
			ax.broken_barh([[span[0], span[1]-span[0]+1] for span in self.topo[qacc]], [0.1, 0.8], color='k')

		s_y = 0
		for record in self.results:
			#if record.query_id.replace('query-', '') != qacc.replace('query-', ''): continue
			print(record)
			ax.set_xlim([0, record.query_length])
			for subject in record.alignments:
				expect = min([hsp.expect for hsp in subject.hsps])
				if expect > self.evalue: continue


				s_y -= 1
				padding = 0.2


				covered = set()
				spans = []
				mismatches = []
				topo = []
				for hsp in subject.hsps:
					hspindices = set(range(hsp.query_start, hsp.query_end+1))
					if covered.intersection(hspindices): continue

					spans.append([hsp.query_start, hsp.query_end])

					mismatches.extend(self.get_mismatches(hsp.query, hsp.match, offset=hsp.query_start))

					covered = covered.union(hspindices)

					if self.hmmtop:
						#print('>>>', subject.hit_id)
						topo.extend(self.project_tms(hsp, self.topo.get(subject.hit_id, [])))

				qstart = np.min(spans)
				ax.broken_barh([[span[0], span[1]-span[0]+1] for span in spans], [s_y+padding, 1-2*padding])

				if self.hmmtop: ax.broken_barh([[span[0], span[1]-span[0]+1] for span in topo], [s_y+padding, 1-2*padding], color='tab:cyan')

				#ax.broken_barh([[span[0], span[1]-span[0]+1] for span in mismatches], [s_y+padding, 1-2*padding], color='tab:orange', alpha=0.5)


				ax.annotate(' ' + subject.hit_id, xy=(qstart, s_y+0.5), color='w', va='center')

		fig.set_figheight((-s_y + 1) * 0.3)
		ax.set_yticks(np.hstack([[1], np.arange(0, s_y-5, -5)]))
		ax.set_ylim([s_y, 1])


		plt.show()

	def project_tms(self, hsp, topo):
		if not topo: return []

		toposet = set()
		for span in topo: toposet = toposet.union(set(range(span[0], span[1]+1)))
		hspset = set(range(hsp.sbjct_start, hsp.sbjct_end+1))

		if not hspset.intersection(toposet): return []

		qresi = hsp.query_start
		sresi = hsp.sbjct_start
		spans = []
		for q, s in zip(hsp.query, hsp.sbjct):

			if sresi in toposet:
				if not spans: spans.append([qresi, qresi])
				elif spans[-1][-1] + 1 == qresi: spans[-1][-1] += 1
				else: spans.append([qresi, qresi])

			if q != '-': qresi += 1
			if s != '-': sresi += 1

		return spans

	def get_plaintext_report(self):
		pass

	def get_html_report(self):
		[self.generate_overview_plot(acc) for acc in self.sequences if acc.startswith('query-')]

if __name__ == '__main__':
	parser = argparse.ArgumentParser()

	blastargs = parser.add_argument_group('BLAST arguments')
	blastargs.add_argument('-query', default='/dev/stdin', help='Query sequence (default: stdin)')

	subjectarg = blastargs.add_mutually_exclusive_group(required=True)
	subjectarg.add_argument('-db', help='BLAST database to BLAST against')
	subjectarg.add_argument('-subject', help='Subject sequence library to BLAST against')

	blastargs.add_argument('-evalue', type=float, default=10, help='E-value threshold (default: 10)')
	blastargs.add_argument('--blastargs', default='', help='Other BLAST arguments')

	args = parser.parse_args()

	blaster = Tcblast(query=args.query, subject=args.subject, db=args.db, evalue=args.evalue, blastargs=shlex.split(args.blastargs))
	blaster.run()
