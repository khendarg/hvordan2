#!/usr/bin/env python

import unittest


class TestTcblast(unittest.TestCase):
	queryfn = 'sequences/family-2.A.123.1.1.faa'
	#subjectfn = 'sequences/family-2.A.123.faa'
	subjectfn = 'sequences/tcdb.faa'
	
	def test_tcblast(self):
		import tcblast

		blaster = tcblast.Tcblast(query=self.queryfn, subject=self.subjectfn)
		blaster.run()

if __name__ == '__main__':
	unittest.main()
