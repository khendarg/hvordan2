#!/usr/bin/env python

import tempfile
import multiquod
import unittest

multiquod.STRICT = True
QUIET = False 
QUIET = True

class TestCreateBlankConfig(unittest.TestCase):
    def test_creation(self):
        blankcfg = multiquod.MultiquodConfig()

    def test_reversibility(self):
        fh = tempfile.NamedTemporaryFile()

        blankcfg = multiquod.MultiquodConfig()
        blankcfg.write(fh)

        fh.flush()
        fh.seek(0)

        reloaded = multiquod.MultiquodConfig.load(fh.name)

        fh.close()

        assert blankcfg == reloaded

    

class TestSinglePlot(unittest.TestCase):
    cfgfn = 'configs/multiquod_single.cfg'
    tmsfn = 'configs/multiquod_tms.cfg'
    multitmsfn = 'configs/multiquod_multitms.cfg'
    savefn = 'configs/multiquod_savesingle.cfg'

    def test_load_config(self):
        ''' test_load_config
        Expected output: Nothing
        Possible issues: Config load/parse errors
        '''
        cfg = multiquod.MultiquodConfig.load(self.cfgfn)

    def test_render(self):
        ''' test_draw
        Expected output: Nothing
        Possible issues: Something going wrong during the preplotting stage
        '''
        cfg = multiquod.MultiquodConfig.load(self.cfgfn)
        cfg.render()
        multiquod.plt.close()

    def test_draw(self):
        ''' test_draw
        Expected output: A hydropathy plot
        '''
        cfg = multiquod.MultiquodConfig.load(self.cfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_save_single(self):
        ''' test_save_single
        Expected output: A new 640x480 file named test.png depicting a hydropathy plot
        Possible issues: No output
        '''
        cfg = multiquod.MultiquodConfig.load(self.savefn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()
    
    def test_tms(self):
        ''' test_tms
        Expected output: A seven-peak hydropathy plot with two orange TMSs near the beginning
        Possible issues: No TMSs
        '''
        cfg = multiquod.MultiquodConfig.load(self.tmsfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_multitms(self):
        ''' test_multitms
        Expected output: A seven-peak hydropathy plot with an orange TMS, a green TMS (actually an orange TMS blended with a cyan TMS), and a cyan TMS in that order
        Possible issues: No TMSs; only two TMSs; all TMSs are orange
        '''
        cfg = multiquod.MultiquodConfig.load(self.multitmsfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_tms_bbox(self):
        ''' test_tms_bbox
        Expected output: A seven-peak hydropathy plot with automatically determined TMSs with an xlim of [0, 268]

        Possible issues: No TMSs; incorrect xlim
        '''
        cfg = multiquod.MultiquodConfig.load(self.cfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)

        assert cfg.subplots['a'].ax.get_xlim() == (0.0, 267.0)
        assert cfg.subplots['a'].ax.get_ylim() == (-3.0, 3.0)

        multiquod.plt.close()

    #def test_wall_bbox(self):
    #    ''' test_wall_bbox
    #    Expected output: A seven-peak hydropathy plot with automatically determined TMSs with an xlim of [0, 267] and a ylim of [-3.0, 4.0] and walls placed at x=20 and x=40

    #    Possible issues: Incorrect lims, rendering errors for walls
    #    '''
    #    cfg = multiquod.MultiquodConfig.load(self.wallfn)
    #    cfg.render()
    #    cfg.draw(quiet=QUIET)

    #    multiquod.plt.close()


class TestAlternatePlots(unittest.TestCase):
    entropycfgfn = 'configs/multiquod_entropy.cfg'
    bothentcfgfn = 'configs/multiquod_bothent.cfg'
    kernelscfgfn = 'configs/multiquod_kernels.cfg'
    nohmmtopcfgfn = 'configs/multiquod_nohmmtop.cfg'

    def test_entropy(self):
        ''' test_entropy
        Expected output: A compositional entropy plot between y=2 and y=5
        Possible issues: Wrong ylim, a hydropathy plot showing up instead
        '''
        cfg = multiquod.MultiquodConfig.load(self.entropycfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_bothent(self):
        ''' test_bothent
        Expected output: A compositional entropy curve and a conformational entropy curve
        Possible issues: 
        Known issues: Both curves are red, making it hard to distinguish between them
        '''
        cfg = multiquod.MultiquodConfig.load(self.bothentcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_kernels(self):
        cfg = multiquod.MultiquodConfig.load(self.kernelscfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_nohmmtop(self):
        cfg = multiquod.MultiquodConfig.load(self.nohmmtopcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class TestMultiplot(unittest.TestCase):
    hdoublecfgfn = 'configs/multiquod_hdouble.cfg'
    hdoubleautocfgfn = 'configs/multiquod_hdouble_auto.cfg'
    vdoublecfgfn = 'configs/multiquod_vdouble.cfg'
    #horizontal/vertical relative to vim split controls, where vsplit is L|R and split is T/B
    equalcfgfn = 'configs/multiquod_equal.cfg'
    proportionalcfgfn = 'configs/multiquod_proportional.cfg'
    lcrtitlecfgfn = 'configs/multiquod_lcrtitle.cfg'
    scalecfgfn = 'configs/multiquod_scale.cfg'

    def test_horizontal_double_plot(self):
        cfg = multiquod.MultiquodConfig.load(self.hdoublecfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_horizontal_double_auto_plot(self):
        cfg = multiquod.MultiquodConfig.load(self.hdoubleautocfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_vertical_double_plot(self):
        cfg = multiquod.MultiquodConfig.load(self.vdoublecfgfn)
        cfg.render()

    def test_equal_allocation(self):
        cfg = multiquod.MultiquodConfig.load(self.equalcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_proportional_allocation(self):
        cfg = multiquod.MultiquodConfig.load(self.proportionalcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_lcrtitle(self):
        cfg = multiquod.MultiquodConfig.load(self.lcrtitlecfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_scale(self):
        cfg = multiquod.MultiquodConfig.load(self.scalecfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class TestMultiple(unittest.TestCase):
    overlaycfgfn = 'configs/multiquod_overlay.cfg'
    def test_overlay(self):
        cfg = multiquod.MultiquodConfig.load(self.overlaycfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class TestLayout(unittest.TestCase):
    figsizecfgfn = 'configs/multiquod_figsize.cfg'
    def test_figsize(self):
        cfg = multiquod.MultiquodConfig.load(self.figsizecfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class TestStackPlot(unittest.TestCase):
    fn = 'sequences/family-2.A.123.1.1.faa'

class TestDrawing(unittest.TestCase):
    drawregioncfgfn = 'configs/multiquod_drawregion.cfg'
    manyregionscfgfn = 'configs/multiquod_manyregions.cfg'
    drawwallcfgfn = 'configs/multiquod_wall.cfg'
    drawpartialcfgfn= 'configs/multiquod_drawpartial.cfg'

    def test_drawregion(self):
        cfg = multiquod.MultiquodConfig.load(self.drawregioncfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_drawmanyregions(self):
        cfg = multiquod.MultiquodConfig.load(self.manyregionscfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_drawwall(self):
        ''' test_drawwall

        Draw black bars with wedges pointing in like old QUOD
        '''
        print('START')
        cfg = multiquod.MultiquodConfig.load(self.drawwallcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_drawpartialregion(self):
        cfg = multiquod.MultiquodConfig.load(self.drawpartialcfgfn)
        cfg.render()
        cfg.draw(quiet=False)
        multiquod.plt.close()

class TestFragment(unittest.TestCase):
    shortn_fragment = 'configs/multiquod_shortnfrag.cfg'
    longn_fragment = 'configs/multiquod_longnfrag.cfg'

    def test_shortnfragment(self):
        cfg = multiquod.MultiquodConfig.load(self.shortn_fragment)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

    def test_longnfragment(self):
        cfg = multiquod.MultiquodConfig.load(self.longn_fragment)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class TestColor(unittest.TestCase):
    colorcfgfn = 'configs/multiquod_coloring.cfg'
    def test_coloring(self):
        cfg = multiquod.MultiquodConfig.load(self.colorcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

class DefaultOptions(unittest.TestCase):
    defaultcfgfn = 'configs/multiquod_defaults.cfg'

    def test_defaults(self):
        cfg = multiquod.MultiquodConfig.load(self.defaultcfgfn)
        cfg.render()
        cfg.draw(quiet=QUIET)
        multiquod.plt.close()

if __name__ == '__main__': unittest.main()
    
