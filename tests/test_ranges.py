#!/usr/bin/env python

import unittest
import libquod.entities

class TestRangeParsing(unittest.TestCase):
    def test_positivepositive(self):
        cases = ("50-150,20-40",
                "50-150",
                "20-40"
                )
        results = [
                [(50.,150.), (20.,40.)],
                [(50., 150.)],
                [(20., 40.)]
                ]

        for case, expected in zip(cases, results):
            returned = libquod.entities.Parser.parse_ranges(case) 
            assert returned == expected

    def test_negativepositive(self):
        cases = ("-50-20,-40-150",
                "-50-20",
                "-40-150")
        results = [
                [(-50., 20.), (-40., 150.)],
                [(-50., 20.)],
                [(-40., 150.)],
                ]

        for case, expected in zip(cases, results):
            returned = libquod.entities.Parser.parse_ranges(case) 
            assert returned == expected

    def test_negativenegative(self):
        cases = ("-50--20,-150--40",
                "-50--20",
                "-150--40",
                )
        results = [
                [(-50., -20.), (-150., -40.)],
                [(-50., -20.)],
                [(-150., -40.)],
                ]

        for case, expected in zip(cases, results):
            returned = libquod.entities.Parser.parse_ranges(case) 
            assert returned == expected

    def test_emptyranges(self):
        assert libquod.entities.Parser.parse_ranges("") == []

        with self.assertRaises(ValueError):
            libquod.entities.Parser.parse_ranges("asdf") == []

        with self.assertRaises(ValueError):
            libquod.entities.Parser.parse_ranges("asdf-") == []

        with self.assertRaises(ValueError):
            libquod.entities.Parser.parse_ranges("-asdf") == []

        with self.assertRaises(ValueError):
            libquod.entities.Parser.parse_ranges("1-asdf") == []

    def test_points(self):
        assert libquod.entities.Parser.parse_ranges("404", allow_points=True) == [(404., 404.)]

        assert libquod.entities.Parser.parse_ranges("20-40,404", allow_points=True) == [(20., 40.), (404., 404.)]

        with self.assertRaises(ValueError):
            libquod.entities.Parser.parse_ranges("404")

class RollingTest(unittest.TestCase):
    def test_unroll(self):
        spans = [(1, 3), (9, 12), (15, 20)]
        expected = [1, 2, 3, 9, 10, 11, 12, 15, 16, 17, 18, 19, 20]
        assert libquod.indexing.unroll_ranges(spans) == expected

    def test_roll(self):
        indices = [1, 2, 3, 9, 10, 11, 12, 15, 16, 17, 18, 19, 20]
        expected = [[1, 3], [9, 12], [15, 20]]
        assert libquod.indexing.roll_ranges(indices) == expected


if __name__ == "__main__":
    unittest.main()
