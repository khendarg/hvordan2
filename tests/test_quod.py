import quod
import unittest
from Bio import SeqIO, AlignIO

QUIET = True

class TestBasic(unittest.TestCase):
    seqfn = 'sequences/family-2.A.123.1.1.faa'
    def test_io(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=QUIET)

    def test_entropy(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=QUIET, modes=['entropy'])

    def test_amphi(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=False, modes=['hydro', 'amphi'])

class TestBatch(unittest.TestCase):
    seqfn = 'sequences/family-2.A.123.1.faa'
    def dont_test_batch(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=QUIET)

class TestPair(unittest.TestCase):
    seqfn1 = 'sequences/family-2.A.123.1.1.faa'
    seqfn2 = 'sequences/family-2.A.123.1.2.faa'
    def test_overlap(self):
        quod.main(*(list(SeqIO.parse(self.seqfn1, 'fasta')) + list(SeqIO.parse(self.seqfn2, 'fasta'))), title='Overlap', quiet=QUIET)

    def test_identity(self):
        quod.main(*(list(SeqIO.parse(self.seqfn1, 'fasta')) + list(SeqIO.parse(self.seqfn2, 'fasta'))), modes=['ident'], title='Identity', quiet=QUIET)

class TestAligned(unittest.TestCase):
    alignedfn = 'sequences/aligned_sweet.faa'

    def test_identity(self):
        quod.main(*SeqIO.parse(self.alignedfn, 'fasta'), modes=['ident'], title='Identity (w=19)', quiet=QUIET)

    def test_identity_shortwindow(self):
        quod.main(*SeqIO.parse(self.alignedfn, 'fasta'), modes=['ident', 'ident'], title='Identity (w=5)', windows=[5,5], quiet=QUIET)

    def test_identity_longwindow(self):
        quod.main(*SeqIO.parse(self.alignedfn, 'fasta'), modes=['ident', 'ident'], title='Identity (w=41)', windows=[41,41], kernels=['hann'], quiet=QUIET)

class TestFragment(unittest.TestCase):
    frag50fn = 'sequences/frag-2.A.123.1.1.faa'
    full50fn = 'sequences/family-2.A.123.1.1.faa'

    def test_frag50(self):
        quod.main(*(list(SeqIO.parse(self.frag50fn, 'fasta')) + list((SeqIO.parse(self.full50fn, 'fasta')))), multi='frag', quiet=QUIET)

    def test_frag50entropy(self):
        quod.main(*(list(SeqIO.parse(self.frag50fn, 'fasta')) + list((SeqIO.parse(self.full50fn, 'fasta')))), modes=['entropy'], multi='frag', quiet=QUIET)

class TestMSA(unittest.TestCase):
    msafn = 'sequences/family-2.A.123.1.aln'
    msatrfn = 'sequences/family-2.A.123.1_tr70.aln'

    def test_msahydrodefault(self):
        quod.main(*(list(AlignIO.parse(self.msafn, 'clustal'))), multi='msa', quiet=QUIET)

    def test_msahydrodefault70(self):
        quod.main(*(list(AlignIO.parse(self.msatrfn, 'clustal'))), multi='msa', quiet=QUIET)

    def test_msahydro(self):
        quod.main(*(list(AlignIO.parse(self.msafn, 'clustal'))), modes=['hydro'], multi='msa', quiet=QUIET)

    def test_msahydrohann(self):
        quod.main(*(list(AlignIO.parse(self.msafn, 'clustal'))), modes=['hydro'], multi='msa', kernels=['hann'], quiet=QUIET)

class TestDraw(unittest.TestCase):
    seqfn = 'sequences/family-2.A.123.1.1.faa'

    def test_wall(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=QUIET, walls=['20-40'])

    def test_dashdomain(self):
        with open(self.seqfn) as fh:
            quod.main(*SeqIO.parse(fh, 'fasta'), quiet=QUIET, addregion=['20-40:red::0.5'])


if __name__ == '__main__':
    unittest.main()
