#!/usr/bin/env python

import unittest
from libquod import indexing
from Bio import SeqIO

QUIET = False

class TestDirectTransfer(unittest.TestCase):
	record100 = SeqIO.read('indexing/test100.faa', 'fasta')
	record100_gappy = SeqIO.read('indexing/test100_gappy.faa', 'fasta')

	def test_self_map(self):
		seq100 = indexing.IndexedSequence(self.record100)

		selfmap = seq100.get_self_map()

		assert len(selfmap) == 100
		assert selfmap[0] == 0
		assert selfmap[99] == 99

	def test_idempotence(self):
		#check that transferring something to itself makes sense
		seq100_0 = indexing.IndexedSequence(self.record100)
		seq100_1 = indexing.IndexedSequence(self.record100)

		selfmap0 = seq100_0.get_self_map()
		selfmap1 = seq100_1.get_self_map()
		crossmap = seq100_0.get_transitive_map(seq100_1)
		assert selfmap0 == selfmap1 == crossmap

	def test_gappy(self):
		seq100_gappy = indexing.IndexedSequence(self.record100_gappy)
		selfmap = seq100_gappy.get_self_map(blacklist=set('-'))

		assert len(selfmap) == 110
		assert selfmap[0] == 0
		assert selfmap[21] == 20
		assert selfmap[22] == 20
		assert selfmap[23] == 20
		assert selfmap[24] == 20
		assert selfmap[25] == 20
		assert selfmap[109] == 99

	def test_direct(self):
		#direct coordinate transfer for a pairwise alignment
		pass

	def test_direct_to_gapless(self):
		#direct coordinate transfer from a gappy sequence to a gapless identical sequence
		pass

	def test_direct_from_gapless(self):
		#direct coordinate transfer from a gapless sequence to a gappy identical sequence
		pass

	def test_direct_to_invalid(self):
		#direct coordinate transfer to a sequence with invalid residues
		pass

	def test_direct_from_invalid(self):
		#direct coordinate transfer from a sequence with invalid residues
		pass

	def test_direct_to_custom_invalid(self):
		#direct coordinate transfer to a sequence with user-specified invalid residues
		pass

	def test_direct_from_custom_invalid(self):
		#direct coordinate transfer from a sequence with user-specified invalid residues
		pass


class TestIndirectTransfer(unittest.TestCase):
	def test_fragment(self):
		#coordinate transfer where the aligned sequence is a fragment of a known sequence
		pass

class TestMSATransfer(unittest.TestCase):
	def test_direct_to_msa(self):
		#coordinate transfer from sequence coordinates to MSA coordinates
		pass

	def test_direct_from_msa(self):
		#coordinate transfer from MSA coordinates to sequences coordinates
		pass

	def test_fragment_to_msa(self):
		#coordinate transfer from sequence fragment coordinates to MSA coordinates
		pass

	def test_fragment_from_msa(self):
		#coordinate transfer from MSA coordinates to sequence fragment coordinates
		pass

class TestPDBTransfer(unittest.TestCase):
	def test_direct_to_pdb(self):
		#coordinate transfer from sequence coordinates to PDB coordinates (including model/chain?)
		pass
	def test_direct_from_pdb(self):
		#coordinate transfer from PDB coordinates (including model/chain?) to sequence coordinates
		pass


class TestHMMTransfer(unittest.TestCase):
	def test_direct_to_hmm(self):
		#coordinate transfer to HMMER HMM coordinates
		pass

	def test_direct_from_hmm(self):
		#coordinate transfer from HMMER HMM coordinates
		pass

	def test_direct_to_hhm(self):
		#coordinate transfer to HH-suite HHM coordinates
		pass

	def test_direct_from_hhm(self):
		#coordinate transfer from HH-suite HHM coordinates
		pass


class TestCoordinateOperations(unittest.TestCase):
	def test_renumber(self):
		#test renumbering from 1
		pass

	def test_proper_renumber(self):
		#test renumbering from 0
		pass

	def test_proper_translate(self):
		#test scalar addition of coordinates
		pass

	def test_insertion(self):
		#test insertion of gaps and such
		pass

	def test_deletion(self):
		#test deletion of positions and such
		pass

if __name__ == '__main__': unittest.main()
